package tasks

import org.hibernate.annotations.Formula
import org.hibernate.annotations.Subselect
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import xtool.xdepjava.tasks.JavaTask
import java.math.BigDecimal
import java.nio.file.Paths

class JavaTaskTestCase {

    @Test
    fun readClassTest() {
        val javaTask = JavaTask()
        val javaClass = javaTask.readClass(Paths.get("src/test/resources/Terceirizado.java"))
        Assertions.assertEquals("Terceirizado", javaClass.name)
    }

    @Test
    fun addClassAnnotationTest() {
        val javaTask = JavaTask()
        val javaClass = javaTask.readClass(Paths.get("src/test/resources/Terceirizado.java"))
        javaTask.addClassAnnotation(javaClass, Subselect::class.java) { it.stringValue = "Select * from TB" }
        println(javaClass)
//        Assertions.assertEquals("Terceirizado", javaClass.name)
    }

    @Test
    fun addFieldAnnotationTest() {
        val javaTask = JavaTask()
        val javaClass = javaTask.readClass(Paths.get("src/test/resources/Terceirizado.java"))
        //javaClassTask.addClassAnnotation(javaClass, Subselect::class.java) { it.stringValue = "Select * from TB" }
        val fieldSource = javaClass.fields[0]
        javaTask.addFieldAnnotation(fieldSource, Formula::class.java) { it.stringValue = "DOCODE(1,2,3)" }
        println(javaClass)
//        Assertions.assertEquals("Terceirizado", javaClass.name)
    }

    @Test
    fun addFieldTest() {
        val javaTask = JavaTask()
        val javaClass = javaTask.readClass(Paths.get("src/test/resources/Terceirizado.java"))
        javaTask.addField(javaClass, "idade") {
            it.setPrivate()
            it.setType(BigDecimal::class.java)
            javaTask.addFieldAnnotation(it, Formula::class.java) { annotationSource -> annotationSource.stringValue = "SELECT 123" }
        }
        println(javaClass)
//        Assertions.assertEquals("Terceirizado", javaClass.name)
    }

    @Test
    fun writeClassTest() {
        val javaTask = JavaTask()
        val javaClass = javaTask.createClass("App", "br.jus")
        javaTask.addField(javaClass, "idade") {
            it.setPrivate()
            it.setType(BigDecimal::class.java)
            javaTask.addFieldTag(it, "@app", "apt-get")
        }
        println(javaClass)
        javaTask.writeClass(Paths.get("/tmp/api/App.java"), javaClass)
//        Assertions.assertEquals("Terceirizado", javaClass.name)
    }
}
