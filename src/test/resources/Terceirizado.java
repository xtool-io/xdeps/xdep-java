package br.jus.tre_pa.app.domain;

import javax.persistence.Entity;
import lombok.Getter;
import lombok.Setter;
import lombok.EqualsAndHashCode;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.SequenceGenerator;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.persistence.OneToMany;
import javax.persistence.JoinColumn;
import javax.persistence.CascadeType;
import java.util.List;
import java.util.ArrayList;
import br.jus.tre_pa.app.domain.HistoricoFuncional;
import org.hibernate.annotations.BatchSize;
import javax.persistence.ManyToOne;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import br.jus.tre_pa.app.domain.databind.SexoDatabind;
import br.jus.tre_pa.app.domain.Sexo;
import br.jus.tre_pa.app.domain.databind.UnidadeDatabind;
import br.jus.tre_pa.app.domain.Unidade;
import br.jus.tre_pa.app.domain.databind.CargoDatabind;
import br.jus.tre_pa.app.domain.Cargo;
import br.jus.tre_pa.app.domain.databind.ContratoDatabind;
import br.jus.tre_pa.app.domain.Contrato;
import br.jus.tre_pa.app.domain.databind.GrauInstrucaoDatabind;
import br.jus.tre_pa.app.domain.GrauInstrucao;

/**
 * @api-path /api/terceirizado
 * @caption Terceirizados
 */
@Entity
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@Table(name = "TERCEIRIZADO")
public class Terceirizado {

	/**
	 * @caption id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_TERCEIRIZADO")
	@SequenceGenerator(initialValue = 1, allocationSize = 1, name = "SEQ_TERCEIRIZADO")
	private Long id;
	/**
	 * @caption Nome
	 */
	@Column(name = "NOME")
	@Size(max = 255)
	private String nome;
	/**
	 * @caption Data de nascimento
	 */
	@Column(name = "DT_NASCIMENTO")
	private LocalDate dtNascimento;
	/**
	 * @caption CPF
	 */
	@Column(name = "CPF")
	@Size(max = 255)
	private String cpf;
	/**
	 * @caption RG
	 */
	@Column(name = "RG")
	@Size(max = 255)
	private String rg;
	/**
	 * @caption Título
	 */
	@Column(name = "TITULO")
	@Size(max = 255)
	private String titulo;
	/**
	 * @caption Tipo Sanguineo
	 */
	@Column(name = "TIPO_SANGUINEO")
	@Size(max = 255)
	private String tipoSanguineo;
	/**
	 * @caption Naturalidade
	 */
	@Column(name = "NATURALIDADE")
	@Size(max = 255)
	private String naturalidade;
	/**
	 * @caption Celular
	 */
	@Column(name = "CELULAR")
	@Size(max = 255)
	private String celular;
	/**
	 * @caption Logradouro
	 */
	@Column(name = "LOGRADOURO")
	@Size(max = 255)
	private String logradouro;
	/**
	 * @caption CEP
	 */
	@Column(name = "CEP")
	@Size(max = 255)
	private String cep;
	/**
	 * @caption Bairro
	 */
	@Column(name = "BAIRRO")
	@Size(max = 255)
	private String bairro;
	/**
	 * @caption Cidade
	 */
	@Column(name = "CIDADE")
	@Size(max = 255)
	private String cidade;
	/**
	 * @caption UF
	 */
	@Column(name = "UF", length = 2)
	@Size(max = 2)
	private String uf;
	/**
	 * @caption dtAdmissao
	 */
	@Column(name = "DT_ADMISSAO")
	private LocalDateTime dtAdmissao;
	/**
	 * @caption dtDesligamento
	 */
	@Column(name = "DT_DESLIGAMENTO")
	private LocalDateTime dtDesligamento;
	/**
	 * @caption Historico Funcional
	 */
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@BatchSize(size = 10)
	@JoinColumn(name = "TERCEIRIZADO_ID", nullable = false)
	private List<HistoricoFuncional> historicoFuncional = new ArrayList<>();
	/**
	 * @caption Terceirizado
	 */
	@ManyToOne(optional = false)
	@JoinColumn(name = "SEXO_ID")
	@JsonDeserialize(using = SexoDatabind.IdDeserializer.class)
	@JsonSerialize(using = SexoDatabind.IdSerializer.class)
	private Sexo sexo;
	/**
	 * @caption Terceirizado
	 */
	@ManyToOne(optional = false)
	@JoinColumn(name = "UNIDADE_ID")
	@JsonDeserialize(using = UnidadeDatabind.IdDeserializer.class)
	@JsonSerialize(using = UnidadeDatabind.IdSerializer.class)
	private Unidade unidade;
	/**
	 * @caption Terceirizado
	 */
	@ManyToOne(optional = false)
	@JoinColumn(name = "CARGO_ID")
	@JsonDeserialize(using = CargoDatabind.IdDeserializer.class)
	@JsonSerialize(using = CargoDatabind.IdSerializer.class)
	private Cargo cargo;
	/**
	 * @caption Terceirizado
	 */
	@ManyToOne(optional = false)
	@JoinColumn(name = "CONTRATO_ID")
	@JsonDeserialize(using = ContratoDatabind.IdDeserializer.class)
	@JsonSerialize(using = ContratoDatabind.IdSerializer.class)
	private Contrato contrato;
	/**
	 * @caption Terceirizado
	 */
	@ManyToOne(optional = false)
	@JoinColumn(name = "GRAUINSTRUCAO_ID")
	@JsonDeserialize(using = GrauInstrucaoDatabind.IdDeserializer.class)
	@JsonSerialize(using = GrauInstrucaoDatabind.IdSerializer.class)
	private GrauInstrucao grauInstrucao;
}