package xtool.xdepjava.tasks

import org.jboss.forge.roaster.Roaster
import org.jboss.forge.roaster.model.source.AnnotationSource
import org.jboss.forge.roaster.model.source.FieldSource
import org.jboss.forge.roaster.model.source.JavaClassSource
import org.jboss.forge.roaster.model.source.JavaInterfaceSource
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.nio.file.Files
import java.nio.file.Path

/**
 * Classe de task que realiza a leitura e manipulação de arquivo *.java.
 */
@Component
class JavaTask {

    private val log = LoggerFactory.getLogger(JavaTask::class.java)

    /**
     * Lê o arquivo [javaPath] e retorna um objeto [JavaClassSource] que representa uma classe Java.
     * @param javaPath Path com o caminho para a classe Java.
     * @return Objeto [JavaClassSource] com a representação da classe Java.
     */
    fun readClass(javaPath: Path): JavaClassSource {
        return Roaster.parseUnit(javaPath.toFile().inputStream()).getGoverningType()
    }

    /**
     * Lê o arquivo [javaPath] e retorna um objeto [JavaInterfaceSource] que representa uma interface Java.
     * @param javaPath Path com o caminho para a classe Java.
     * @return Objeto [JavaInterfaceSource] com a representação da interface Java.
     */
    fun readInterface(javaPath: Path): JavaInterfaceSource {
        return Roaster.parseUnit(javaPath.toFile().inputStream()).getGoverningType()
    }

    /**
     * Verifica se o arquivo definido em [javaPath] é uma classe Java.
     */
    fun isClass(javaPath: Path): Boolean {
        return Roaster.parseUnit(javaPath.toFile().inputStream()).getGoverningType().isClass
    }

    /**
     * Verifica se o arquivo definido em [javaPath] é uma interface Java.
     */
    fun isInterface(javaPath: Path): Boolean {
        return Roaster.parseUnit(javaPath.toFile().inputStream()).getGoverningType().isInterface
    }

    /**
     * Grava uma classe java no sistema de arquivos.
     * @param javaPath Path com o caminho onde a classe será salva.
     * @param javaClassSource Objeto [JavaClassSource] com a representação da classe Java.
     */
    fun writeClass(javaPath: Path, javaClassSource: JavaClassSource) {
        if (Files.notExists(javaPath.parent)) Files.createDirectories(javaPath.parent)
        val os = Files.newOutputStream(javaPath)
        os.write(Roaster.format(javaClassSource.toUnformattedString()).toByteArray())
        os.flush()
        os.close()
        log.debug("Classe salva em: {}", javaPath)
    }

    /**
     * Cria um classe Java.
     * @param name String com o nome simples (Sem o pcacote) da classe Java.
     * @param packageName String com o nome do pacote
     * @return Objeto [JavaClassSource] com a representação da classe Java.
     */
    fun createClass(
        name: String,
        packageName: String
    ): JavaClassSource {
        return Roaster.create(JavaClassSource::class.java).apply {
            this.name = name
            this.`package` = packageName
        }
    }

    /**
     * Adiciona uma annotation à classe Java.
     * @param javaClassSource Objeto [JavaClassSource] que representa uma classe Java.
     * @param annotationClass Classe da annotation a ser adicionada à classe
     * @param annotationSourceFn Lambda que possui como único parâmetro o objeto [AnnotationSource] na qual serão definidas as propriedades da annotation a ser adicionada.
     */
    fun <T : Annotation> addClassAnnotation(
        javaClassSource: JavaClassSource,
        annotationClass: Class<T>,
        annotationSourceFn: (annotationSource: AnnotationSource<JavaClassSource>) -> Unit = {},
    ) {
        val annotationSource = javaClassSource.annotations.find { it.name == annotationClass.simpleName } ?: javaClassSource.addAnnotation(annotationClass)
        annotationSource.apply(annotationSourceFn)
    }

    /**
     * Adiciona um atributo à classe.
     * @param javaClassSource Objeto [JavaClassSource] que representa uma classe Java.
     * @param name String com o nome do atributo
     * @param fieldSourceFn Lambda que possui como único parâmetro o objeto [FieldSource] na qual serão definidas as propriedades do atributo a ser adicionada.
     * @return Objeto [FieldSource] representado o atributo da classe recém criado.
     */
    fun addField(
        javaClassSource: JavaClassSource,
        name: String,
        fieldSourceFn: (fieldSource: FieldSource<JavaClassSource>) -> Unit
    ): FieldSource<JavaClassSource> {
        val fieldSource = javaClassSource.fields.find { it.name == name } ?: javaClassSource.addField().setName(name)
        fieldSource.apply(fieldSourceFn)
        return fieldSource
    }

    /**
     * Adiciona uma annotation ao atributo da classe.
     * @param fieldSource Objeto [FieldSource] que representa um atributo de classe.
     * @param annotationClass Classe da annotation a ser adicionada ao atributo
     * @param annotationSourceFn Lambda que possui como único parâmetro o objeto [AnnotationSource] na qual serão definidas as propriedades da annotation a ser adicionada.
     */
    fun <T : Annotation> addFieldAnnotation(
        fieldSource: FieldSource<JavaClassSource>,
        annotationClass: Class<T>,
        annotationSourceFn: (annotationSource: AnnotationSource<JavaClassSource>) -> Unit = {},
    ) {
        val annotationSource = fieldSource.annotations.find { it.name == annotationClass.simpleName } ?: fieldSource.addAnnotation(annotationClass)
        annotationSource.apply(annotationSourceFn)
    }

    /**
     * Adiciona uma tag ao atributo.
     *  @param fieldSource Objeto [FieldSource] que representa um atributo de classe.
     *  @param key String com o nome da tag
     *  @param value String com o valor da tag
     */
    fun addFieldTag(
        fieldSource: FieldSource<JavaClassSource>,
        key: String,
        value: String
    ) {
        val tag = fieldSource.javaDoc.tags.find { it.name == key } ?: fieldSource.javaDoc.addTagValue(key, value)
    }


}
