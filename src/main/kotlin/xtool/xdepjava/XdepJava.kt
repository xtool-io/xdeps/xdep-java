package xtool.xdepjava

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import javax.annotation.PostConstruct

@Configuration
@ComponentScan
class XdepJava {
    private val log = LoggerFactory.getLogger(XdepJava::class.java)

    @PostConstruct
    fun init() {
        log.debug("xdep-java carregado com sucesso.")
    }
}
